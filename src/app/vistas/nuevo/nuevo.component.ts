import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../servicios/api/api.service';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {ListaEstudianteI} from '../../modelos/listaestudiante.interface';
import {ResponseI} from '../../modelos/response.interface';
import {Router} from '@angular/router';

@Component({
  selector: 'app-nuevo',
  templateUrl: './nuevo.component.html',
  styleUrls: ['./nuevo.component.css']
})
export class NuevoComponent implements OnInit {

  nombre= '';
  image= '';
  patronus= '';
  age= '';

  constructor() {
    this.obtenerLocalStorage();
   }
   
  obtenerLocalStorage(){

    this.nombre = localStorage.getItem("name");
    this.image = localStorage.getItem("image");
    this.patronus = localStorage.getItem("patronus");
    this.age = localStorage.getItem("age");

  }


  ngOnInit(): void {
  }

}
