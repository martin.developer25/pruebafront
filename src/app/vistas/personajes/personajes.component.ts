import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../servicios/api/api.service';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {ListaEstudianteI} from '../../modelos/listaestudiante.interface';
import {Router} from '@angular/router';

@Component({
  selector: 'app-personajes',
  templateUrl: './personajes.component.html',
  styleUrls: ['./personajes.component.css']
})
export class PersonajesComponent implements OnInit {

  lista:string[]=["slytherin", "gryffindor", "ravenclaw", "hufflepuff"];
  personaje: ListaEstudianteI[];
  eleccionCasa = new FormGroup({
    casa : new FormControl('', Validators.required),
  });

  constructor(private api: ApiService, private router: Router) { }

  ngOnInit(): void {
    this.api.getAllCasa("gryffindor").subscribe(data=> {
      this.personaje = data;
    });
  }

  elegirCasa(form){
    console.log(form);
    this.api.getAllCasa(form.casa).subscribe(data=> {
      this.personaje = data;
    });
  }

}
