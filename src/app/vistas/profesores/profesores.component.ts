import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../servicios/api/api.service';
import {ListaEstudianteI} from '../../modelos/listaestudiante.interface';
import {Router} from '@angular/router';

@Component({
  selector: 'app-profesores',
  templateUrl: './profesores.component.html',
  styleUrls: ['./profesores.component.css']
})
export class ProfesoresComponent implements OnInit {

  profesor: ListaEstudianteI[];

  constructor(private api: ApiService, private router: Router) { }

  ngOnInit(): void {
    this.api.getAllProf().subscribe(data=> {
      this.profesor = data;
    });
  }

}
