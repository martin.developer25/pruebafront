import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../servicios/api/api.service';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {ListaEstudianteI} from '../../modelos/listaestudiante.interface';
import {ResponseI} from '../../modelos/response.interface';
import {Router} from '@angular/router';

@Component({
  selector: 'app-estudiantes',
  templateUrl: './estudiantes.component.html',
  styleUrls: ['./estudiantes.component.css']
})
export class EstudiantesComponent implements OnInit {

  estudianteForm = new FormGroup({
    name : new FormControl('', Validators.required),
    patronus: new FormControl('', Validators.required),
    age: new FormControl('', Validators.required),
    image: new FormControl('', Validators.required),
  });

  estudiante: ListaEstudianteI[];

  constructor(private api: ApiService, private router: Router) { }

  ngOnInit(): void {
    this.api.getAllEstudiante().subscribe(data=> {
      this.estudiante = data;
    });
  }

  onLogin(form)
  {
    localStorage.setItem("name", form.name);
    localStorage.setItem("patronus", form.patronus);
    localStorage.setItem("age", form.age);
    localStorage.setItem("image", form.image);
    this.router.navigate(["nuevo"]);

      //this.router.navigate(["nuevo"]);

    
  }

}
