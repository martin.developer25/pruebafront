import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DashboardComponent} from './vistas/dashboard/dashboard.component';
import {PersonajesComponent} from './vistas/personajes/personajes.component';
import {EstudiantesComponent} from './vistas/estudiantes/estudiantes.component';
import {ProfesoresComponent} from './vistas/profesores/profesores.component';
import {NuevoComponent} from './vistas/nuevo/nuevo.component';

const routes: Routes = [
  {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'personajes', component: PersonajesComponent},
  {path: 'estudiante', component: EstudiantesComponent},
  {path: 'profesores', component: ProfesoresComponent},
  {path: 'nuevo', component: NuevoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [DashboardComponent, EstudiantesComponent, PersonajesComponent, ProfesoresComponent]
