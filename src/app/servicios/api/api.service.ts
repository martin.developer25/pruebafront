import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ListaEstudianteI} from '../../modelos/listaestudiante.interface';
import {ResponseI} from '../../modelos/response.interface';
import { Observable } from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  url: string = 'http://hp-api.herokuapp.com/api/characters/'

  constructor(private http: HttpClient) { }

  loginEstudiante(form: ListaEstudianteI):Observable<ResponseI>{
    let direccion = this.url + "students"; 
    return this.http.post<ResponseI>(direccion, form);
  }

  getAllEstudiante():Observable<ListaEstudianteI[]>{

    let direccion = this.url + "students";

    return this.http.get<ListaEstudianteI[]>(direccion);
  }

  getAllProf():Observable<ListaEstudianteI[]>{

    let direccion = this.url + "staff";

    return this.http.get<ListaEstudianteI[]>(direccion);
  }
  
  getAllCasa(casa: string):Observable<ListaEstudianteI[]>{

    let direccion = this.url + "house/" + casa;

    return this.http.get<ListaEstudianteI[]>(direccion);
  }
  

}
